import React, { Fragment } from 'react'
import Link from 'next/link'
import Head from 'next/head'

const Xmas = () => (
  <Fragment>
    <Head>
      <title>Merry X-mas!</title>
    </Head>
    <section className="Xmas">
      <h1>Frohe Festtage 2020</h1>
      <video controls autoplay muted width="100%">
        <source src="/videos/98d754cd1c1bacdb26316422c4194c10.mp4" type="video/mp4" />
        Sorry, your browser doesn't support embedded videos.
      </video>
      <small>Ton anmachen!</small>
    </section>
  </Fragment>
)

export default Xmas
