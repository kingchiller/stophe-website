import { AppProps } from 'next/app'
import Router from 'next/router'
import NProgress from 'nprogress'
import { Flipper, Flipped } from 'react-flip-toolkit'
import PlausibleProvider from 'next-plausible'

import '@assets/main.scss'
import '@assets/nprogress.scss'
import '@assets/markdown.scss'

import Layout from '@components/Layout'

Router.events.on('routeChangeStart', url => {
  console.log(`Loading: ${url}`)
  NProgress.start()
})
Router.events.on('routeChangeComplete', () => NProgress.done())
Router.events.on('routeChangeError', () => NProgress.done())

const MyApp = ({ Component, pageProps, router }: AppProps) => {
  return (
    <PlausibleProvider domain="stophe.com">
      <Layout>
        <Flipper flipKey={`${router.pathname}`}>
          <Flipped flipId="layout" translate spring="wobbly">
            <div>
              <Component {...pageProps} />
            </div>
          </Flipped>
        </Flipper>
      </Layout>
    </PlausibleProvider>
  )
}

export default MyApp
