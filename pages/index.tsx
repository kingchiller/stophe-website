import React from 'react'
import { ArrowRight } from 'react-bootstrap-icons'
import Link from 'next/link'
import Image from 'next/legacy/image'

import styles from './index.module.scss'

const Home = () => {
  return (
    <section className={styles.Index}>
      <div className="mb-3">
        <Image className="rounded" src="/images/me.jpg" width={120} height={120} />
      </div>
      <h1>Hi, I'm Christophe</h1>

      <p className="lead mb-5">
        My day job is at{' '}
        <a href="https://pax.ch" target="_blank">
          Pax
        </a>{' '}
        where I help build digital products for the web. At night, I work on various side projects,
        notably{' '}
        <a href="https://sharrr.com" target="_blank">
          sharrr.com
        </a>{' '}
        and{' '}
        <a href="https://scrt.link" target="_blank">
          scrt.link
        </a>
        .
      </p>
      <div className={`${styles.Links} d-flex flex-column align-items-start`}>
        <Link href="/work" className="d-inline-flex align-items-center py-1">
          <ArrowRight className="mr-2" />
          Work
        </Link>
        <Link href="/about" className="d-inline-flex align-items-center py-1">
          <ArrowRight className="mr-2" />
          About me
        </Link>
        <Link href="/contact" className="d-inline-flex align-items-center py-1">
          <ArrowRight className="mr-2" />
          Get in touch
        </Link>
      </div>
    </section>
  )
}

export default Home
