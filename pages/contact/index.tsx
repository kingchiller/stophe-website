import { ContactMenu } from '@components/Navigation'

import styles from './index.module.scss'

const Contact = () => (
  <div className="pt-5">
    <h1 className="page-header">Contact</h1>
    <p className="lead">I'm occasionally open to new projects. Feel free to reach out!</p>
    <p className="lead">
      <a href="mailto:x@stophe.com">x@stophe.com</a>
    </p>
    <ContactMenu className={styles.menu} />
  </div>
)

export default Contact
