import React, { Fragment } from 'react'
import Link from 'next/link'
import Head from 'next/head'
import ReactMarkdown from 'react-markdown'
import { ArrowLeftShort } from 'react-bootstrap-icons'
import { GetStaticProps } from 'next'

import { WorkItem } from '@pages/work'
import { getWork } from '@utils/work'

type PostPageProps = {
  workItem: WorkItem
}
const PostPage = ({ workItem }: PostPageProps) => (
  <Fragment>
    <Head>
      <title>{workItem.title}</title>
    </Head>

    <div className="pt-5 pb-3">
      <Link href="/work" style={{ display: 'inline-flex', alignItems: 'center' }}>

        <ArrowLeftShort className="icon" />Back to Work
      </Link>
    </div>

    <section className="markdown mb-5">
      <h1>{workItem.title}</h1>

      <ReactMarkdown source={workItem.body} escapeHtml={false} />
    </section>

    <div>
      <Link href="/work" style={{ display: 'flex', alignItems: 'center' }}>

        <ArrowLeftShort className="icon" />Back to Work
      </Link>
    </div>
  </Fragment>
)

export async function getStaticPaths() {
  const data = await getWork()

  const paths = data.map(workItem => ({
    params: { slug: workItem.slug },
  }))

  return { paths, fallback: false }
}

export const getStaticProps: GetStaticProps = async ({ params = {} }) => {
  const data = await getWork()

  const workItem = data.find(item => item.slug === params.slug)

  return {
    props: { workItem },
  }
}

export default PostPage
