import React, { useState, useEffect } from 'react'
import Link from 'next/link'
import { Flipper, Flipped, spring } from 'react-flip-toolkit'
import { isNil, pipe, uniq, intersection } from 'ramda'
import classNames from 'classnames'

import { GetStaticProps } from 'next'

import styles from './index.module.scss'
import { formatDate } from '@utils/super'
import TagList from '@components/TagList'
import { getWork } from '@utils/work'

const formatPostDate = (date: string) => {
  const year = formatDate(date, { year: 'numeric' })
  const month = formatDate(date, { month: 'numeric' })

  return new Date().getFullYear() - Number(year) > 3 ? year : `${month}/${year}`
}

export interface WorkItem {
  body: string
  date: string
  fileName: string
  preview: string
  slug: string
  tags: string[]
  title: string
  isHidden?: boolean
}

type WorkProps = {
  sortedPosts: WorkItem[]
}
type SettingsProps = {
  type: 'list' | 'grid'
  tags: string[]
  spring: 'stiff' | 'noWobble' | 'veryGentle' | 'gentle' | 'wobbly'
}
const Work = ({ sortedPosts }: WorkProps) => {
  const [settings, setSettings] = useState({
    type: 'list',
    tags: [],
    spring: 'veryGentle',
  } as SettingsProps)

  const initialTagCount = 3
  const [tagCount, setTagCount] = useState(1)
  const [posts, setPosts] = useState([] as WorkItem[])

  const updateFilteredTags = (tag: string | null, allowMultiple?: boolean) => {
    let tags = settings.tags

    if (isNil(tag)) {
      tags = []
    } else {
      if (allowMultiple) {
        if (tags.includes(tag)) {
          tags = tags.filter(item => item !== tag)
        } else {
          tags.push(tag)
        }
      } else {
        tags = []
        tags.push(tag)
      }
    }

    setPosts(
      tags.length ? sortedPosts.filter(item => intersection(item.tags, tags).length) : sortedPosts,
    )
    setSettings({
      ...settings,
      tags,
    })
  }

  const sortArrayByOccurences = (arr: string[]) =>
    arr.sort((a, b) => arr.filter(v => v === a).length - arr.filter(v => v === b).length)

  const allTags = sortedPosts.map(item => item.tags).flat()
  const tagsSortedByOccurences = pipe(sortArrayByOccurences, uniq)(allTags).reverse()

  const onElementAppear = (el: HTMLElement, index: number) =>
    spring({
      onUpdate: val => {
        el.style.opacity = val.toString()
      },
      delay: index * 50,
    })

  useEffect(() => {
    setTagCount(initialTagCount)
    setPosts(sortedPosts)
  }, [])

  return (
    <section className={styles.Index}>
      <Flipper
        flipKey={`${settings.tags.join('-')}-${tagCount}`}
        spring={settings.spring}
        staggerConfig={{
          card: {
            speed: 0.5,
          },
        }}
        decisionData={settings}
      >
        <div>
          <div>
            <TagList
              tags={tagsSortedByOccurences}
              selectedTags={settings.tags}
              onSelect={updateFilteredTags}
              tagCount={tagCount}
              onMoreClick={() =>
                setTagCount(
                  tagCount === initialTagCount ? tagsSortedByOccurences.length : initialTagCount,
                )
              }
            />
          </div>

          <ul className={styles.PostList}>
            {posts.map(({ slug, preview, tags, title, date }, index) => (
              <Flipped key={index} flipId={slug} stagger="card" onAppear={onElementAppear}>
                <li key={slug}>
                  <Link
                    href="/work/[slug]"
                    as={`/work/${slug}`}
                    className={styles['PostList-post']}>

                    <div className={styles['Image']}>
                      <img
                        loading="lazy"
                        src={`/static/images/posts/thumbs/${preview}`}
                        alt={title}
                      />
                    </div>
                    <div className={styles['Meta']}>
                      <span className={classNames(styles.Title, 'nowrap')}>{title}</span>
                      <span className={styles.Description}>
                        <ul className="nowrap">
                          {tags.map(tag => (
                            <li className={styles.Tag} key={tag}>
                              {tag}
                            </li>
                          ))}
                        </ul>
                        <time dateTime={date}>{formatPostDate(date)}</time>
                      </span>
                    </div>

                  </Link>
                </li>
              </Flipped>
            ))}
          </ul>
        </div>
      </Flipper>
    </section>
  );
}

export const getStaticProps: GetStaticProps = async () => {
  const data = await getWork()
  const sortedPosts = data.sort((a, b) => (a.fileName < b.fileName ? 1 : -1))

  return {
    props: { sortedPosts: sortedPosts.filter(item => !item.isHidden) }, // will be passed to the page component as props
  }
}

export default Work
