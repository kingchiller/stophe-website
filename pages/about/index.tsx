import Link from 'next/link'
import { ArrowRight } from 'react-bootstrap-icons'

const tech = [
  'HTML, (S)CSS, JS (Typescript)',
  'Svelte',
  'React, Next.js',
  'Vue, Nuxt.js',
  'Angular JS, Analog JS',
  'Tailwind, Bootstrap, MUI (Material), Chakra UI, Storybook',
  'Redux, Vuex, Ramda',
]
const design = [
  'Creation of Design Systems',
  'Illustrator',
  'Figma, Sketch',
  'Photoshop',
  'Midjourney',
  'Blender',
]

const Who = () => (
  <div className="markdown pt-5">
    <h1 className="page-header">About Me</h1>
    <p className="lead">["Developer", "Designer", "Dad"].sort()</p>
    <p>
      I am currently a team lead at Pax. My ambition is to transform the product development process
      in order to create outstanding digital experiences for our customers. My journey began as a
      child when I first discovered the internet — a fascinating place that opened a window to the
      world and offered access to endless knowledge. That initial encounter sparked a never-ending
      curiosity about design and technology. To create unique experiences on a screen - that's what
      motivated me to build my first website in high school. It's the reason I taught myself coding,
      it's what inspired me to study art and design. Over the years, I tried to evenly split my
      attention between design and code - it became my mission to understand the distinct challenges
      of each field and to ultimately become an intermediary between the two worlds, fostering
      collaboration and innovation.
    </p>
    <p>
      Past roles: Frontend Lead, Senior Frontend Developer, UI Architect, Designer, Founder,
      Consultant
    </p>
    <Link href="/contact" className="d-inline-flex align-items-center">
      <ArrowRight className="mr-2" />
      Request CV
    </Link>

    <h2>Principles</h2>
    <ul>
      <li>
        <strong>Keep curious</strong>: Delivering state-of-the-art web solutions requires continuous
        evolution. I invest considerable time in researching and experimenting with new
        technologies.
      </li>
      <li>
        <strong>Be a minimalist</strong>: <q>Less, but better</q>, <q>Work smart, not hard</q>,{' '}
        <q>Don't repeat yourself</q>, <q>Form follows function</q> - There are many quotes that
        circumscribe what I call being a minimalist.
      </li>
      <li>
        <strong>Open source</strong>: For obvious reasons. For me personally, the strongest argument
        is the idea of shared knowledge — the ability to learn from others.
      </li>
      <li>
        <strong>Accessibility</strong>: The web is a great place; let everyone participate!
      </li>
      <li>
        <strong>People and privacy over profits</strong>
      </li>
    </ul>

    <h2>Technologies and Skills</h2>
    <p className="lead">List of most recently used tools and technologies.</p>
    <Link href="/work" className="d-inline-flex align-items-center">
      <ArrowRight className="mr-2" />
      Work showcase
    </Link>

    <h3>Frontend</h3>
    <ul>
      {tech.map(item => (
        <li key={item}>{item}</li>
      ))}
    </ul>

    <h3>Design</h3>
    <ul>
      {design.map(item => (
        <li key={item}>{item}</li>
      ))}
    </ul>

    <h3>Management</h3>
    <p>
      As a team lead, I help shape the vision for digital product ecosystems. I am fluent in German
      and English, and <em>un petit peu de français</em>. As a founder, I also understand the
      challenges of running a business.
    </p>
  </div>
)

export default Who
