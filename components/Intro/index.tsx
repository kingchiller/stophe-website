import { Fragment, useEffect, useState } from 'react'
import Typewriter from 'typewriter-effect'
import classNames from 'classnames'
import { ArrowClockwise } from 'react-bootstrap-icons'

import { useStateWithLocalStorage } from '@utils/super'
import styles from './index.module.scss'

const Intro = () => {
  const [value, setValue] = useStateWithLocalStorage('showIntro')

  // Force re-hydration on client
  const [showIntro, setShowIntro] = useState(false)
  useEffect(() => {
    setShowIntro(value !== 'false')
  }, [value])

  return (
    <div className={styles.Wrapper}>
      <button
        className={classNames('button-reset', styles.Backdrop, {
          [styles['Backdrop--visible']]: showIntro,
        })}
        onClick={() => setValue(String(false))}
      >
        <small>Skip Intro</small>
      </button>
      <h1
        className={classNames(styles.Heading, {
          [styles['Heading--with-backdrop']]: showIntro,
        })}
      >
        {showIntro ? (
          <Typewriter
            options={{
              autoStart: true,
              delay: 'natural',
              cursor: '▌',
            }}
            onInit={typewriter => {
              typewriter
                .pauseFor(1200)
                .deleteAll(1)
                .typeString(`Hi`)
                .pauseFor(1900)
                .typeString(`, `)
                .pasteString(`I'm `, null)
                .typeString(`Chris`)
                .pauseFor(1000)
                .changeDelay(100)
                .typeString(`, <br/>UI designer.`)
                .pauseFor(200)
                .changeDeleteSpeed(1)
                .deleteChars(12)
                .typeString(`front-end `)
                .pasteString(`web `, null)
                .pauseFor(200)
                .typeString(`dev`)
                .pasteString(`eloper.`, null)
                .pauseFor(1200)
                .deleteChars(17)
                .pauseFor(1200)
                .typeString('!')
                .pauseFor(2900)
                .callFunction(function (state) {
                  state.elements.cursor.style.visibility = 'hidden'
                  setValue(String(false))
                })
                .start()
            }}
          />
        ) : (
          <Fragment>
            Hi, I'm Chris!{' '}
            <button
              className="button-reset text-muted"
              onClick={() => {
                setValue(String(true))
              }}
            >
              <ArrowClockwise />
            </button>
          </Fragment>
        )}
      </h1>
    </div>
  )
}

export default Intro
