import React, { Fragment, createContext } from 'react'

import Head from 'next/head'

import Header from '@components/Header'
import Navigation from '@components/Navigation'
import { useStateWithLocalStorage } from '@utils/super'

import styles from './index.module.scss'

interface LayoutContext {
  isNightMode: boolean
  setIsNightMode: any
}
const initialState = {
  isNightMode: false,
  setIsNightMode: (_value: string): void => {},
}

export const LayoutContext = createContext(initialState)

const LayoutContextProvider: React.FunctionComponent = ({ children }) => {
  const [value, setValue] = useStateWithLocalStorage('isNightMode')

  return (
    <LayoutContext.Provider value={{ isNightMode: value === 'true', setIsNightMode: setValue }}>
      {children}
    </LayoutContext.Provider>
  )
}

const jsonLd = {
  '@context': 'https://www.schema.org',
  '@type': 'Person',
  name: 'Chistophe Schwyzer',
  email: 'x@stophe.com',
  affiliation: ['Denkmal.org, SANTiHANS GmbH, Creadi AG, Taktwerk GmbH'],
  knowsLanguage: 'en, de, fr',
  jobTitle: 'Senior frontend web developer',
  gender: 'https://schema.org/Male',
  description: 'Senior frontend web developer and user interface designer.',
  worksFor: ['Denkmal.org, SANTiHANS GmbH, Creadi AG, Taktwerk GmbH'],
  url: 'https://www.linkedin.com/in/christophe-schwyzer-19b9193b',
}

const Layout: React.FunctionComponent = ({ children }) => (
  <Fragment>
    <Head>
      <title>stophe - Developer, Designer, Dad</title>
      <meta
        name="description"
        content="Christophe Schwyzer - developer, designer. @stophecom on social media."
        key="description"
      />
      <meta
        name="keywords"
        content="Christophe Schwyzer, stophe, stophecom, frontend web developer, designer, user interface expert, santihans, Basel, Switzerland"
        key="keywords"
      />
      <script
        dangerouslySetInnerHTML={{ __html: JSON.stringify(jsonLd) }}
        type="application/ld+json"
      />

      <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png" />
      <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png" />
      <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png" />
      <link rel="manifest" href="/site.webmanifest" />
      <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5" />
      <meta name="msapplication-TileColor" content="#ffffff" />
      <meta name="theme-color" content="#ffffff"></meta>
    </Head>
    <div className={styles.Wrapper}>
      <LayoutContextProvider>
        <Header />

        <main className={styles.Main} id="page">
          <div className="container">{children}</div>
        </main>

        <Navigation />
      </LayoutContextProvider>
    </div>
  </Fragment>
)

export default Layout
