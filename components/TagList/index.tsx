import React from 'react'
import { Flipped, spring } from 'react-flip-toolkit'
import classNames from 'classnames'
import { take } from 'ramda'

import styles from './index.module.scss'

type TagListProps = {
  tags: string[]
  selectedTags: string[]
  tagCount: number
  onSelect: (tag: string | null) => void
  onMoreClick: () => void
}
const TagList: React.FunctionComponent<TagListProps> = ({
  tags,
  selectedTags = [],
  tagCount,
  onSelect,
  onMoreClick,
}) => {
  const onElementAppear = (el: HTMLElement, index: number) =>
    spring({
      onUpdate: val => {
        el.style.opacity = val.toString()
      },
      delay: index * 50,
    })

  return (
    <div className={styles.TagList}>
      <button
        className={classNames(styles['TagList-tag'], 'button-reset px-2 py-1 m-1', {
          [styles['TagList-tag--active']]: !selectedTags.length,
        })}
        onClick={() => onSelect(null)}
      >
        All work
      </button>
      {[...take(tagCount)(tags), `extraElement-${tagCount}`].map((item, index) => {
        return (
          <Flipped key={index} flipId={`id-${item}`} onAppear={onElementAppear}>
            <div className="d-inline-flex">
              {index === tagCount ? (
                <button
                  className={classNames(styles['TagList-more'], 'button-reset ml-2 text-muted')}
                  onClick={onMoreClick}
                >
                  {tagCount < tags.length ? 'More tags' : 'Short list'}
                </button>
              ) : (
                <button
                  key={index}
                  className={classNames(styles['TagList-tag'], 'button-reset px-2 py-1 m-1', {
                    [styles['TagList-tag--active']]: selectedTags.includes(item),
                  })}
                  onClick={() => onSelect(item)}
                >
                  #{item}
                </button>
              )}
            </div>
          </Flipped>
        )
      })}
    </div>
  )
}

export default TagList
