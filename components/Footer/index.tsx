import React from 'react'

import styles from './index.module.scss'

const Footer = () => (
  <footer className={styles.Wrapper}>
    <div className="container d-flex align-items-center">
      <small className="text-muted">©&thinsp;{new Date().getFullYear()} Christophe Schwyzer</small>
    </div>
  </footer>
)

export default Footer
