import React, { useState, useEffect } from 'react'
import Link from 'next/link'
import classNames from 'classnames'
import { throttle } from 'throttle-debounce'
import { useRouter } from 'next/router'
import Typewriter from 'typewriter-effect'

import { NavigationMenu } from '@components/Navigation'

import NightModeSwitch from '@components/NightModeSwitch'

import styles from './index.module.scss'

type LogoProps = {
  className?: string
}
export const Logo: React.FunctionComponent<LogoProps> = ({ className }) => (
  <Link href="/" className={classNames(styles.logo, className)}>
    ~stophe
  </Link>
)
const Header = () => {
  const [showScrollShadow, setShowScrollShadow] = useState(false)
  const [routeChange, setRouteChange] = useState(true)

  const checkScrollPosition = throttle(100, () => setShowScrollShadow(window.scrollY > 10))

  const router = useRouter()

  useEffect(() => {
    window.addEventListener('scroll', checkScrollPosition)

    return () => {
      window.removeEventListener('scroll', checkScrollPosition)
    }
  }, [])

  useEffect(() => {
    if (router.pathname.includes('work')) {
      return
    }

    setRouteChange(true)
  }, [router])

  const regex = /(?<!^)\/.*/i // Look for second slash '/' that is not in the beginning
  const breadcrumb = router.asPath.replace(regex, '')

  return (
    <div className={styles.wrapper}>
      <header
        className={classNames(styles.bar, {
          [styles['bar--scrolled']]: showScrollShadow,
        })}
      >
        <div className="container">
          <div className={styles['bar-inner']}>
            <Logo className={styles['logo-header']} />
            <span className={styles.breadcrumb}>
              {router.pathname === '/' ? (
                ''
              ) : routeChange ? (
                <Typewriter
                  options={{
                    delay: 'natural',
                    cursor: '▌',
                  }}
                  onInit={typewriter => {
                    typewriter
                      .deleteAll(1)
                      .typeString(breadcrumb)
                      .callFunction(function (state) {
                        state.elements.cursor.style.visibility = 'hidden'
                        setRouteChange(false)
                      })
                      .start()
                  }}
                />
              ) : (
                breadcrumb
              )}
            </span>

            <NavigationMenu className={styles.menu} activeClassName={styles['link--active']} />

            <div className={styles.right}>
              <NightModeSwitch className={styles.NightModeSwitch} />
            </div>
          </div>
        </div>
      </header>
    </div>
  )
}

export default Header
