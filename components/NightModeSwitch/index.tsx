import React, { useEffect, useState, useContext } from 'react'
import classNames from 'classnames'
import { Moon, BrightnessHighFill } from 'react-bootstrap-icons'

import { LayoutContext } from '@components/Layout'

import styles from './index.module.scss'

type NightModeSwitchProps = {
  showlabel?: boolean
  className?: string
}
const NightModeSwitch: React.FunctionComponent<NightModeSwitchProps> = ({
  className,
  showlabel,
}) => {
  const { setIsNightMode, isNightMode } = useContext(LayoutContext)

  // Force re-hydration on client
  const [isNightModeActive, setIsNightModeActive] = useState(false)

  useEffect(() => {
    if (isNightMode) {
      document.documentElement.classList.add('theme--night')
    } else {
      document.documentElement.classList.remove('theme--night')
    }
    setIsNightModeActive(isNightMode)
  }, [isNightMode])

  return (
    <button
      className={classNames(styles.button, 'button-reset text-muted', className)}
      onClick={() => {
        return setIsNightMode(String(!isNightModeActive))
      }}
    >
      {isNightModeActive ? <Moon /> : <BrightnessHighFill />}
      {showlabel && <span className="ml-2">{isNightModeActive ? 'Dark mode' : 'Light mode'}</span>}

      <span className="sr-only">
        {isNightModeActive
          ? 'Night mode active. Click to switch to daylight mode.'
          : 'Daylight mode active. Click to switch to night mode.'}
      </span>
    </button>
  )
}
export default NightModeSwitch
