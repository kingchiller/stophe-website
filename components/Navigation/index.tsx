import React, { useEffect, useState } from 'react'
import Link from 'next/link'
import classNames from 'classnames'
import { useRouter } from 'next/router'

import NightModeSwitch from '@components/NightModeSwitch'

import styles from './index.module.scss'

type ClassNameProp = {
  className?: string
}

type MenuProps = {
  label: string
  url: string
}

const menu = [
  {
    href: '/work',
    label: 'Work',
  },
  {
    href: '/about',
    label: 'About',
  },
  {
    href: 'https://blog.stophe.com/',
    label: 'Blog',
    external: true,
  },
  {
    href: '/contact',
    label: 'Contact',
  },
]

const social = [
  { label: 'Mastodon', url: 'https://mastodon.social/@stophecom' },
  { label: 'Bluesky', url: 'https://bsky.app/profile/stophe.com' },
  {
    label: 'LinkedIn',
    url: 'https://www.linkedin.com/in/christophe-schwyzer-19b9193b/',
  },
]

const repositories = [
  { label: 'Gitlab', url: 'https://gitlab.com/kingchiller' },
  { label: 'Github', url: 'https://www.github.com/stophecom' },
]

type NavigationMenuProps = {
  className?: string
  activeClassName?: string
}
export const NavigationMenu: React.FunctionComponent<NavigationMenuProps> = ({
  className,
  activeClassName,
}) => {
  const router = useRouter()

  return (
    <nav className={className}>
      <ul>
        {menu.map(({ href, label, external }, index) => (
          <li key={index}>
            {external ? (
              <a href={href} target="_blank" rel="noopener">
                {label}
              </a>
            ) : (
              <Link
                href={href}
                key={index}
                className={classNames({ [`${activeClassName}`]: router.pathname === href })}
              >
                {label}
              </Link>
            )}
          </li>
        ))}
      </ul>
    </nav>
  )
}

const IconMenu: React.FunctionComponent<ClassNameProp & { menuItems: MenuProps[] }> = ({
  menuItems,
  className,
}) => (
  <nav className={className}>
    {menuItems.map(({ label, url }) => (
      <a key={url} href={url} target="_blank" rel="me noopener" className="py-1">
        {label}
      </a>
    ))}
  </nav>
)

export const SocialLinks: React.FunctionComponent<ClassNameProp> = ({ className }) => (
  <IconMenu menuItems={social} className={classNames(styles.social, className)} />
)

export const ContactMenu: React.FunctionComponent<ClassNameProp> = ({ className }) => (
  <IconMenu
    menuItems={[...social, ...repositories]}
    className={classNames('d-flex flex-column', className)}
  />
)

const Navigation = () => {
  const router = useRouter()
  const [isActive, setIsActive] = useState(false)

  useEffect(() => {
    router.events.on('routeChangeStart', () => {
      setIsActive(false)
    })
  }, [])

  return (
    <div className={styles.wrapper}>
      <div className={classNames(styles.navigation, { [styles['navigation--active']]: isActive })}>
        <div
          className={classNames('d-grid justify-content-center align-items-center', styles.menu)}
        >
          <NavigationMenu className={styles['menu-main']} />

          <NightModeSwitch showlabel />
        </div>
      </div>
      <button
        onClick={() => setIsActive(!isActive)}
        className={classNames('button-reset', styles.hamburger, {
          [styles['hamburger--active']]: isActive,
        })}
      >
        <span className="sr-only">Open Menu</span>
      </button>
    </div>
  )
}

export default Navigation
