---
title: Denkmalmit
tags: ['Web']
preview: 'denkmalmit.png'
slug: denkmalmit
---

Denkmalmit ist the organisation behind Denkmal.org, the event calendar for Basel's night life. [denkmalmit.org](https://denkmalmit.org).

## Screenshots

![Denkmalmit](/static/images/posts/media/denkmalmit/website.png)
![Denkmalmit](/static/images/posts/media/denkmalmit/website-mobile.png)

## Technology

- Illustrator
- Vue.js
- Nuxt.js
- Zeit now
