---
title: SANTiHANS
tags: ['Branding', 'Web', 'Marketing']
preview: 'santihans.svg'
slug: 'santihans'
---

Corporate Design for SANTiHANS GmbH, the agency I founded. [santihans.com](https://www.santihans.com).

## Webdesign

![SANTiHANS](/static/images/posts/media/santihans/website-index.png)
![SANTiHANS](/static/images/posts/media/santihans/website-index-1.png)
![SANTiHANS](/static/images/posts/media/santihans/website-index-2.png)
![SANTiHANS](/static/images/posts/media/santihans/website-blog.png)
![SANTiHANS](/static/images/posts/media/santihans/website-mobile.png)

## Technology

- Nuxt.js
- Netlify
