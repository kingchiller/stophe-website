---
title: Drawing
tags: ['Drawing']
preview: 'drawing.jpg'
slug: 'drawings'
---

Sketches.

![Drawing](/static/images/posts/media/drawing/Drawing1.jpg)
![Drawing](/static/images/posts/media/drawing/Drawing2.jpg)
![Drawing](/static/images/posts/media/drawing/Drawing3.jpg)
![Drawing](/static/images/posts/media/drawing/Drawing4.jpg)
![Drawing](/static/images/posts/media/drawing/Drawing5.jpg)
