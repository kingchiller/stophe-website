---
title: Gundeli Street Team
tags: ['Branding']
preview: 'gundeli-street-team.png'
slug: 'gundeli-street-team'
---

Logo for Gundeli Street Team - Basel running community.

![Gundeli Street Team](/static/images/posts/media/gundeli-street-team/GST-Man.png)
![Gundeli Street Team](/static/images/posts/media/gundeli-street-team/GST-Woman.png)

## Resources

- Illustrator
