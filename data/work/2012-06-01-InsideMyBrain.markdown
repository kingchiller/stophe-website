---
title: Inside my brain
tags: ['Visualisation']
preview: 'brain.png'
slug: brain-visualisation
---

Take a look inside my head ;)

<iframe width="853" height="480" src="https://www.youtube.com/embed/o7t-qajcxzk" frameborder="0" allowfullscreen></iframe>

## Resources

- MRI-Data
- [MeVisLab](https://www.mevislab.de/)
- Adobe AE
