---
title: Sculpture
tags: ['Objects']
preview: 'sculpture.png'
slug: 'sculpture'
---

Cut out from a cuboid of gypsum.

![Sculpture](/static/images/posts/media/sculpture/Sculpture1.jpg)
![Sculpture](/static/images/posts/media/sculpture/Sculpture2.jpg)
