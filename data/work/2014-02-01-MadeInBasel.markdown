---
title: Made in Basel
tags: ['Web']
preview: 'made-in-basel.png'
slug: made-in-basel
---

Open source project to connect and support the developer community in Basel. Project hosted on [Github](https://github.com/MadeInBasel/madeinbasel.github.io).

![Made in Basel](/static/images/posts/media/made-in-basel/MadeInBasel-Website.png)
![Made in Basel](/static/images/posts/media/made-in-basel/MadeInBasel-Badges.png)

## Resources

- Illustrator
- html, css
