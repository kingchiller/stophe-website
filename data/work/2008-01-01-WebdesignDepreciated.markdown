---
title: 'Old stuff'
tags: ['Illustrator', 'Web']
preview: 'webdesign.png'
slug: 'old-stuff'
---

Some old web design drafts that are either outdated or were never implemented.

![Webdesign Depreciated](/static/images/posts/media/webdesign/Webdesign-BBB.jpg)
![Webdesign Depreciated](/static/images/posts/media/webdesign/Webdesign-BBB2.jpg)
![Webdesign Depreciated](/static/images/posts/media/webdesign/Webdesign-ApartmentSever.jpg)
![Webdesign Depreciated](/static/images/posts/media/webdesign/Webdesign-JessicaBuschor.jpg)
![Webdesign Depreciated](/static/images/posts/media/webdesign/Webdesign-Lernwerk.jpg)
![Webdesign Depreciated](/static/images/posts/media/webdesign/Webdesign-SpitzenOpenair.jpg)
![Webdesign Depreciated](/static/images/posts/media/webdesign/Webdesign-Voxel.jpg)

## Resources

- Illustrator
- Photoshop
- Flash
- html, css
