---
title: Marguerite
tags: ['Branding']
preview: 'marguerite.png'
slug: marguerite
hidden: true
---

Logo for a raft project.

![Marguerite](/static/images/posts/media/marguerite/Marguerite-Logo1.png)

## Resources

- Photoshop
- Illustrator
