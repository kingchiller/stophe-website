---
title: LED Paper Lamp
tags: ['Objects']
preview: 'led.png'
slug: led-lamp
---

This Lamp is mainly made out of paper. The half transparent paper gives a nice light, combined with the irregular squares, a real nice effect with almost no costs. Downside of course is that you can barely touch it and its broken ;)

![LED Lamp](/static/images/posts/media/led/LEDPaperLamp.png)

## Resources

- 25 white LEDs
- AC to DC 12 V Adapter
- some resistors, cables
- paper 90 g/m2
