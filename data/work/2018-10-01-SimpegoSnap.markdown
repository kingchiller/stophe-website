---
title: Simpego Snap
tags: ['Web']
preview: 'simpego-snap.jpg'
slug: simpego-snap
---

Front-end development and UI design for an award winning car insurance web-app. With the app you could take you a picture of your vehicle registration document and get the right car insurance in return.

## Screenshots

![Simpego Snap](/static/images/posts/media/simpego-snap/website-mobile.png)

## Technology

- Nuxt.js
- Vue
- Netlify
