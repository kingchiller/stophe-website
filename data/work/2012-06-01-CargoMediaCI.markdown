---
title: Cargo Media CI
tags: ['Branding', 'Web']
preview: 'cargomedia.png'
slug: cargomedia
---

Create and develop the Cargo Media Corporate Identity. (Including website implementation.) See full website: [www.cargomedia.ch](https://www.cargomedia.ch).

## Webdesign

![Cargo Media Website](/static/images/posts/media/cargomedia/CargoMedia-App.jpg)
![Cargo Media Website](/static/images/posts/media/cargomedia/CargoMedia-Website.png)

## Logo

![Cargo Media Logo](/static/images/posts/media/cargomedia/CargoMedia-Logo.png)
