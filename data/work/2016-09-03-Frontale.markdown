---
title: Frontâle Festival 2
tags: ['Web', 'Branding', 'Marketing']
preview: 'frontale-2016.png'
slug: frontale-festival-2016
---

Second Edition: [Frontâle Festival](https://www.frontale.ch) - French Music Festival 2016

## Webdesign

![Frontale](/static/images/posts/media/frontale-2016/Frontale-App.jpg)

## Print

![Frontale](/static/images/posts/media/frontale-2016/Frontale-Postcard.png)
![Frontale](/static/images/posts/media/frontale-2016/Frontale-Impression-1.jpg)
![Frontale](/static/images/posts/media/frontale-2016/Frontale-Impression-2.jpg)

## Resources

- Illustrator
