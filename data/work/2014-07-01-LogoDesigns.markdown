---
title: Logo Designs
tags: ['Branding']
preview: 'schiff-mode.png'
slug: logo-designs
---

Logo for a the organisation "Schiff Mode".

![Schiff Mode](/static/images/posts/media/schiff-mode/SchiffMode-Logo.png)

![Art44](/static/images/posts/media/art44/Art44-Logo.png)

## Resources

- Illustrator
