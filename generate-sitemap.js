const fs = require('fs')
const matter = require('gray-matter')
const R = require('ramda')
const path = require('path')
const read = require('read-directory')

const raw = read.sync(path.join(process.cwd(), 'data/work'), { transform: matter })

let work = []

R.forEachObjIndexed((value, key) => {
  work.push({
    slug: R.pathOr(key, ['data', 'slug'])(value),
  })
})(raw)

const baseUrl = 'https://stophe.com/'

const renderRoutes = ({ loc, lastmod, changefreq, priority }) => {
  const contents = [`<loc>https://stophe.com/${loc}</loc>`]
  if (lastmod) {
    contents.push(`<lastmod>${lastmod}</lastmod>`)
  }
  if (changefreq) {
    contents.push(`<changefreq>${changefreq}</changefreq>`)
  }
  if (priority) {
    contents.push(`<priority>${priority}</priority>`)
  }
  return `<url>${contents.join('')}</url>`
}

const routes = work.map(({ slug }) => renderRoutes({ loc: `work/${slug}` }))

routes.push(renderRoutes({ loc: '' }))

const sitemap = `<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="https://www.sitemaps.org/schemas/sitemap/0.9">
${routes.join('\n')}
</urlset>`

fs.writeFile('./public/sitemap.xml', sitemap, function (err) {
  if (err) {
    return console.log(err)
  }

  console.log('Sitemap was saved!')
})
