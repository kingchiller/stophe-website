import { useState, useEffect } from 'react'

// import { LocalStorage } from 'node-localstorage'
const globalAny: any = global

export const getDateFromSlug = (slug: string): string => {
  // 2008-01-01-WebdesignDepreciated -> 2008-01-01
  const regex = /[\d]{4}-[\d]{2}-[\d]{2}/g
  const match = slug.match(regex)
  return match?.length ? match[0] : ''
}

export const formatDate = (isoDate: string, options: Intl.DateTimeFormatOptions) => {
  const date = new Date(isoDate)
  return date.toLocaleDateString('de-CH', options)
}

export const useStateWithLocalStorage = (localStorageKey: string) => {
  // Polyfill
  if (typeof localStorage === 'undefined' || localStorage === null) {
    globalAny.localStorage = {
      _data: {},
      setItem: function (id: string, val: string | boolean) {
        return (this._data[id] = val)
      },
      getItem: function (id: string) {
        return this._data.hasOwnProperty(id) ? this._data[id] : undefined
      },
      removeItem: function (id: string) {
        return delete this._data[id]
      },
      clear: function () {
        return (this._data = {})
      },
    }
  }

  const [value, setValue] = useState(localStorage.getItem(localStorageKey) || '')

  useEffect(() => {
    localStorage.setItem(localStorageKey, value)
  }, [value])

  return [value, setValue] as const
}
