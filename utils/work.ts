import { forEachObjIndexed, pathOr } from 'ramda'

import matter from 'gray-matter'
import path from 'path'
import read from 'read-directory'

import { getDateFromSlug } from '@utils/super'

import { WorkItem } from '@pages/work'

const raw = read.sync(path.join(process.cwd(), 'data/work'), { transform: matter })
const work: WorkItem[] = []

forEachObjIndexed((value, key) => {
  work.push({
    body: pathOr('', ['content'])(value),
    date: getDateFromSlug(key),
    fileName: key,
    preview: pathOr('', ['data', 'preview'])(value),
    slug: pathOr(key, ['data', 'slug'])(value),
    tags: pathOr([], ['data', 'tags'])(value),
    title: pathOr('', ['data', 'title'])(value),
    isHidden: pathOr(false, ['data', 'hidden'])(value),
  })
})(raw)

export const getWork = (): Promise<Array<any>> => Promise.resolve(work)
