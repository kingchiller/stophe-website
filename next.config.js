const path = require('path')

const withPlugins = require('next-compose-plugins')
const { withPlausibleProxy } = require('next-plausible')

const withBundleAnalyzer = require('@next/bundle-analyzer')({
  enabled: process.env.ANALYZE === 'true',
})

const plugins = [withPlausibleProxy, withBundleAnalyzer]

const config = {
  webpack(config) {
    config.module.rules.push({
      test: /\.markdown|md$/,
      use: ['raw-loader'],
    })

    config.resolve.alias['@components'] = path.resolve(__dirname, 'components')
    config.resolve.alias['@assets'] = path.resolve(__dirname, 'assets')
    config.resolve.alias['@data'] = path.resolve(__dirname, 'data')
    config.resolve.alias['@utils'] = path.resolve(__dirname, 'utils')
    config.resolve.alias['@store'] = path.resolve(__dirname, 'store')

    return config
  },
}

module.exports = withPlugins(plugins, config)
