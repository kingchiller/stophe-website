# Personal website powered by next.js

www.stophe.com

## How it works

```js
// Install dependencies
pnpm i

// Run dev
pnpm dev

// Deploy preview
vercel

// Deploy production
vercel --prod

```
